﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Loggly_logger_class
{
    public class Log
    {
        /// <summary>
        /// Uploads en error to loggly
        /// </summary>
        /// <param name="message">The message you want to display</param>
        /// <param name="location">Where in the code did the error happen?</param>
        /// <param name="textdump">The error message</param>
        /// <param name="tags">The tags for identifying the log</param>
        public void LogError(string message, string location, string textdump, string tags)
        {
            //StringBuilder JsonString = new StringBuilder();
            //JsonString.Append(@"{""type"":""Error");
            //JsonString.Append(@",""message"":" + message);
            //JsonString.Append(@",""location"":" + location);
            //JsonString.Append(@",""other"":" + textdump);
            var data = new NameValueCollection();
            data["type"] = "Error";
            data["message"] = message;
            data["location"] = location;
            data["other"] = textdump;
            sendLog(data, tags);
        }

        /// <summary>
        /// Uploads a log to salesforce
        /// </summary>
        /// <param name="message">The message you wish to be displayed</param>
        /// <param name="textdump">Optional textdump</param>
        /// <param name="tags">The tags for identifying the log</param>
        public void logJob(string message, string textdump, string tags)
        {
            var data = new NameValueCollection();
            data["type"] = "Log";
            data["message"] = message;
            if (!String.IsNullOrEmpty(textdump))
            {
                data["other"] = textdump;
            }
            sendLog(data, tags);
        }

        /*
        private static void sendLog(string message, string tags)
        {
            WebRequest req = WebRequest.Create(@"http://logs-01.loggly.com/inputs/b8a65059-8298-4cf5-9ac2-02330a32e8be/tag/net" + tags);
            req.Method = "POST";
            //req.ContentType = @"application/xml; charset=utf-8";
            req.ContentType = @"application/json; charset=utf-8";
            req.ContentLength = Encoding.UTF8.GetByteCount(message);
            //req.Headers.Add("Date", DateTime.Now.ToString());
            using (Stream stream = req.GetRequestStream())
            {
                stream.Write(Encoding.UTF8.GetBytes(message), 0, Encoding.UTF8.GetByteCount(message));
            }
            HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
        }
        */

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="tags"></param>
        private void sendLog(NameValueCollection data, string tags)
        {
            string url = @"http://logs-01.loggly.com/inputs/b8a65059-8298-4cf5-9ac2-02330a32e8be/tag/net" + tags;
            using (var wb = new WebClient())
            {
                var response = wb.UploadValues(url, "POST", data);
                Console.WriteLine(System.Text.Encoding.Default.GetString(response));
            }
        }
    }
}
